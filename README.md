### 1 JavaScript: Introduction
    * JS is a scripting language run in the browser
    * EcmaScript is a standard. 
        - EcmaScript current standard: EcmaScript 2015 (ES6)
        - Newest version of EcmaScript is EcmaScript 2017 (ES8), but is no real support
    * Jquery
        - A JS library
    * JavaScript framework  
        - Front-end application used to simplify the building of web-based applications
        - Ex: AngularJS, React, Vue.js
    * Node.js
        - A JS platform allow use JavaScript on server site.
### 2 The basic
    * Browser console allow to run JS command
    * There are 2 ways to use JS:
        - Inline JS:
            + If want to run JS before content is rendered: add to the <head>
            + If want to run JS after content is rendered: add to the <body>
        - External JS files
            + Create .js file
            + <script src="path_to_js_file"></script>
    * Render blocking: while JS file downloading& executing, html page stop rendering their content
        - The 2 solutions:     
            + Using async option: js files downloading during page content rendering, render blocking only when JS files executing
            + Using defer option: js files only execute when html content has finished render content
    * To write good JS:
        - JS is case sensitive
        - Use camelCase
        - Whitespace matters (to human)
        - End each statement with ; 
        - Use comment liberally
### 3 Working with data      
    * Create variable with var prefix to make it local scope
    * With out var prefix, variables become global scope
    * JS is weakly type language, users don't have to declare type of content
    * Data types in JS:
        - Numeric
        - String
        - Boolean
        - null: store intentional absence of a value
        - undefined: get when create variable without set any value to it
        - Symbol         
    * JS arithmetic & math operators: 
        - Include: +,-,*,/,=    
        - Special case with ++/--
            + Put ++/-- before variables name will add/minus 1 to its values first then do the command
            + Put ++/-- after variables name will do the command first, and add/minus 1 to its value
        - Using + for a numeric with a string, then the result will be a string combining. Ex: 4+'5' = '45'
        - Using -,*,/ users will get a number result instead.   
    * Logic conditions    
        - == and ===
            + ==: More lenient. (same absolute value)
            + ===: Absolute strict. (same data type) 
        - Combine 2 or more conditions
            + &&: and
            + ||: or
        - Ternary operator:
            logic_condition ? <command when true> : <command when false>;
    * Array:
        - List of thing is stored together and marked with an index;
        - Index of array started with 0;
        - Array is Object
        - Array have property length, show total number of thing stored in. 
        - More reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array\

### 4 Function & Object
    * 3 types of function:
        - Named function
        - Anonymous function
        - Immediately invoke function expression
    * const and let (since ES6)
        - const: value can't be changed
        - let:
            + Block scope variables
            + Smaller scop than var                  
    * Object:   
        - Can be defined like normal function
    * Dot and Bracket notation
        - Use . for common property
        - User [] when some weird property name occur
    * Closure
        - Function inside a function, but relies on variables from the outside function to work. 
### 5 JavaScript and the DOM, #1 Changing DOM elements
    * Window is top level object of Browser Object Model (BOM)
    * To get elements inside document body:
        - getElementById
        - getElementByName
        - getElementByTagName
        - querySelector
        - querySelectorAll    
    * Access to change elements property:
        - innerHTML
        - outerHTML
    * Access to class:
        - There are 5 method: add, remove, toggle, item, contains
    * Access to attribute:
        - hasAttribute
        - getAttribute
        - setAttribute
        - removeAttribute
    *  Adding new DOM elements:
        - Create the elements
        - Create the text node that goes inside the element
        - Add the text node to element
        - Add the element to the DOM tree        
    * Creation methods:
        - createElement : create an element
        - createTextNode: create text node
        - appendChild: place one child node inside another
    * Add inline CSS
        - Use selector to select and use .style.{CSS property} to modify
        - CSS is also an attribute, so can use attribute accessing method to modify css
### 7 JavaScript and the DOM, #2 Events
    Some of common events:
    * Browser events
        - Load
        - Error
        - Online/ Offline
        - Resize
        - Scroll
    * DOM events
        - Focus
        - Blur
        - Reset/submits
        - Mouse
    * Other events:
        - Media events
        - Progress events
        - CSS transition events 
### 9 Loops
    * There 2 ways of loop:
        - For loop
        - While loop
    * Break & Continue
        - Break : terminate current loop
        - Continue: terminate current iteration of the loop
### 10 Automated Responsive Images Markup
    * Use sizes and srcset attribute
    
### 11 Troubleshooting, Validating, and Minifying JavaScript        
    * Debugging using web console
    * Script linting:
        - Online tools:
            + JSLint
            + JSHint
        - Automate tools:
            + ESLint    
    * Script minifying:
        - Online tools: 
            + Minifier.org
        - Automate tools:
            + uglify js        